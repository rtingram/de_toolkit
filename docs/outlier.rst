``outlier`` - Outlier Identification
====================================

Functions for identifying outlier genes/samples.
