``filter`` - Filtering Count Matrices
=====================================

Functions for filtering count matrices based on various criteria.
