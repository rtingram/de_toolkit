``transform`` - Count Transformation
====================================

Transformations of the distribution of counts in a matrix.
